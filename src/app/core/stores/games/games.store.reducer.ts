import { createReducer, on } from '@ngrx/store';
import { Game, OTHER_CATEGORIES } from '../../models';
import {
  getJackpotGamesError,
  getJackpotGamesSuccess,
  retrieveGames,
  retrieveGamesError,
  retrieveGamesSuccess,
} from './games.store.action';
import { cloneDeep } from 'lodash';

export interface GamesState {
  games: Game[];
  categories: string[];
  action: GamesStoreAction;
  error: unknown;
  loading: boolean;
}

export enum GamesStoreAction {
  NO_ACTION = 'NO_ACTION',
  RETRIEVE_GAMES = 'RETRIEVE_GAMES',
  RETRIEVE_GAMES_SUCCESS = 'RETRIEVE_GAMES_SUCCESS',
  RETRIEVE_GAMES_ERROR = 'RETRIEVE_GAMES_ERROR',
  GET_JACKPOT_GAMES_SUCCESS = 'GET_JACKPOT_GAMES_SUCCESS',
  GET_JACKPOT_GAMES_ERROR = 'GET_JACKPOT_GAMES_ERROR',
}

const initialState: GamesState = {
  games: [],
  action: GamesStoreAction.NO_ACTION,
  categories: ['top', 'new', 'jackpot'],
  error: '',
  loading: false,
};

export const gamesReducer = createReducer(
  initialState,
  on(retrieveGames, (state) => {
    state = {
      ...state,
      action: GamesStoreAction.RETRIEVE_GAMES,
      loading: true,
    };

    return state;
  }),
  on(retrieveGamesSuccess, (state, action) => {
    const uniqueCategories = new Set<string>(state.categories);

    action.games.forEach((game) =>
      game.categories.forEach((category) => {
        if (!OTHER_CATEGORIES.includes(category))
          uniqueCategories.add(category);
      })
    );

    uniqueCategories.add('others');

    state = {
      ...state,
      action: GamesStoreAction.RETRIEVE_GAMES_SUCCESS,
      games: action.games,
      categories: [...uniqueCategories],
      loading: false,
    };

    return state;
  }),
  on(retrieveGamesError, (state, action) => {
    state = {
      ...state,
      action: GamesStoreAction.RETRIEVE_GAMES_ERROR,
      error: action.error,
      loading: false,
    };
    return state;
  }),
  on(getJackpotGamesSuccess, (state, action) => {
    action.jackpots.forEach((jackpot) => {
      const games = cloneDeep(state.games);
      let index = state.games.findIndex((game) => game.id === jackpot.game);

      if (index !== -1) {
        games[index] = new Game({ ...games[index], amount: jackpot.amount });

        state = { ...state, games };
      }
    });

    state = {
      ...state,
      action: GamesStoreAction.GET_JACKPOT_GAMES_SUCCESS,
    };

    return state;
  }),
  on(getJackpotGamesError, (state, action) => {
    state = {
      ...state,
      action: GamesStoreAction.GET_JACKPOT_GAMES_ERROR,
      error: action.error,
    };
    return state;
  })
);

import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { GamesApiService } from '../../services';
import { GamesEffects } from './games.store.effect';
import { gamesReducer } from './games.store.reducer';

@NgModule({
  imports: [
    EffectsModule.forFeature([GamesEffects]),
    StoreModule.forFeature('games', gamesReducer),
  ],
  providers: [GamesApiService],
})
export class GamesStoreModule {}

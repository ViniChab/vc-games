import { Injectable, inject } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import {
  exhaustMap,
  map,
  catchError,
  of,
  interval,
  concat,
  switchMap,
} from 'rxjs';
import { Game } from '../../models';
import { GamesApiService } from '../../services';
import {
  retrieveGames,
  retrieveGamesSuccess,
  retrieveGamesError,
  getJackpotGamesSuccess,
} from './games.store.action';

@Injectable()
export class GamesEffects {
  private actions$: Actions<Action> = inject(Actions);
  private apiService = inject(GamesApiService);

  retrieveGames$ = createEffect(() =>
    this.actions$.pipe(
      ofType(retrieveGames),
      exhaustMap(() =>
        this.apiService.getGames().pipe(
          map((res) => {
            res = res.map((game) => new Game(game));
            return retrieveGamesSuccess({ games: res });
          }),
          catchError((error) => of(retrieveGamesError({ error })))
        )
      )
    )
  );

  getJackpotGames$ = createEffect(() =>
    this.actions$.pipe(
      ofType(retrieveGamesSuccess),
      switchMap(() =>
        concat(of(null), interval(3000)).pipe(
          exhaustMap(() =>
            this.apiService
              .getJackpotGames()
              .pipe(map((res) => getJackpotGamesSuccess({ jackpots: res })))
          )
        )
      )
    )
  );
}

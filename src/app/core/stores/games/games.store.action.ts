import { createAction, props } from '@ngrx/store';
import { Game, JackpotGame } from '../../models';

export const retrieveGames = createAction('[Games] Retrieve Games');

export const retrieveGamesSuccess = createAction(
  '[Games] Retrieve Games Success',
  props<{ games: Game[] }>()
);

export const retrieveGamesError = createAction(
  '[Games] Retrieve Games Error',
  props<{ error: unknown }>()
);

export const getJackpotGamesSuccess = createAction(
  '[Games] Get Jackpot Games Success',
  props<{ jackpots: JackpotGame[] }>()
);

export const getJackpotGamesError = createAction(
  '[Games] Get Jackpot Games Error',
  props<{ error: unknown }>()
);

import { createFeatureSelector, createSelector } from '@ngrx/store';
import { GamesState } from './games.store.reducer';

export const gamesState = createFeatureSelector<GamesState>('games');

export const getCategories = createSelector(
  gamesState,
  (state) => state.categories
);

export const isLoading = createSelector(gamesState, (state) => state.loading);

export const getGamesByCategories = (category: string) =>
  createSelector(gamesState, (state) =>
    state.games.filter((game) => game.categories.includes(category))
  );

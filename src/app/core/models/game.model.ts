export const OTHER_CATEGORIES = ['ball', 'virtual', 'fun'];

export class Game {
  public id!: string;
  public name!: string;
  public image!: string;
  public categories!: string[];
  public new!: boolean;
  public top!: boolean;
  public amount?: number;

  constructor(init: Partial<Game>) {
    Object.assign(this, init);

    this.new = init.categories?.includes('new') || false;
    this.top = init.categories?.includes('top') || false;

    if (this.categories?.some((cat) => OTHER_CATEGORIES.includes(cat))) {
      this.categories = [...this.categories, 'others'];
    }

    if (this.amount) {
      this.categories = [...this.categories, 'jackpot'];
    }
  }
}

export interface JackpotGame {
  game: string;
  amount: number;
}

import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { MatIconModule } from '@angular/material/icon';
import { RibbonComponent } from '../../../../components/ribbon/ribbon.component';
import { Game } from '../../../../models';

@Component({
  selector: 'app-game-item',
  templateUrl: './game-item.component.html',
  styleUrls: ['./game-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [CommonModule, MatIconModule, RibbonComponent],
})
export class GameItemComponent {
  @Input() public game!: Game;
  @Input() public currentCategory!: string;
}

import { CommonModule } from '@angular/common';
import {
  ChangeDetectionStrategy,
  Component,
  inject,
  Input,
  OnChanges,
  SimpleChanges,
} from '@angular/core';
import { Store } from '@ngrx/store';
import {
  GamesState,
  GamesStoreModule,
  getGamesByCategories,
} from '../../stores/games';
import { GameItemComponent } from './components/game-item/game-item.component';
import { MatTabsModule } from '@angular/material/tabs';
import { Observable } from 'rxjs';
import { Game } from '../../models';

@Component({
  selector: 'app-game-list',
  templateUrl: './game-list.component.html',
  styleUrls: ['./game-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [CommonModule, GamesStoreModule, GameItemComponent, MatTabsModule],
})
export class GameListComponent implements OnChanges {
  @Input() public category!: string;
  public games$!: Observable<Game[]>;
  private gamesStore = inject(Store<GamesState>);

  ngOnChanges(simpleChanges: SimpleChanges): void {
    if (simpleChanges['category']) {
      this.getGames();
    }
  }

  public trackGame(index: number, game: Game): string {
    return game.id;
  }

  private getGames() {
    this.games$ = this.gamesStore.select(getGamesByCategories(this.category));
  }
}

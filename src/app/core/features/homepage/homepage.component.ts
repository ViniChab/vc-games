import { ChangeDetectionStrategy, Component, inject } from '@angular/core';
import { Store } from '@ngrx/store';
import {
  GamesState,
  getCategories,
  isLoading,
  retrieveGames,
} from '../../stores/games';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HomepageComponent {
  private gamesStore = inject(Store<GamesState>);
  public categories$ = this.gamesStore.select(getCategories);
  public loading$ = this.gamesStore.select(isLoading);

  ngOnInit(): void {
    this.gamesStore.dispatch(retrieveGames());
  }
}

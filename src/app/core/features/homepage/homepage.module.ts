import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomepageComponent } from './homepage.component';
import { Routes, RouterModule } from '@angular/router';
import { GameListComponent } from '../game-list/game-list.component';
import { MatTabsModule } from '@angular/material/tabs';
import { TranslocoModule } from '@ngneat/transloco';
import { PageLoadingComponent } from '../../components/page-loading/page-loading.component';

const routes: Routes = [
  {
    path: '',
    component: HomepageComponent,
  },
];

@NgModule({
  declarations: [HomepageComponent],
  imports: [
    CommonModule,
    MatTabsModule,
    TranslocoModule,
    RouterModule.forChild(routes),
    GameListComponent,
    PageLoadingComponent,
  ],
})
export class HomepageModule {}

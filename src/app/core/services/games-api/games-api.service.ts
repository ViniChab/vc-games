import { HttpClient } from '@angular/common/http';
import { inject, Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environment/environment';
import { Game, JackpotGame } from '../../models';

@Injectable()
export class GamesApiService {
  private http: HttpClient = inject(HttpClient);

  public getGames(): Observable<Game[]> {
    return this.http.get<Game[]>(environment.GAMES_URL);
  }

  public getJackpotGames(): Observable<JackpotGame[]> {
    return this.http.get<JackpotGame[]>(environment.JACKPOT_URL);
  }
}
